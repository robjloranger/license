// Package license generates a license text with the current year
// and copywrite holder or equivalent's name. In some cases the project title
// and or a short description are also required.
//
// The package returns a string and an error, if any.
package license

import (
	"bytes"
	"strings"
	"text/template"
	"time"
)

// Gen returns a populated license and an error.
// t is required, args should be a struct of type license.Values
func Gen(t string, d Values) (string, error) {
	var buf []byte              // bytes for buffer
	out := bytes.NewBuffer(buf) // buffer for writing temlate to
	t = strings.ToUpper(t)      // allow for case insensitive acronym or name
	// set the year
	if d.Year == 0 {
		d.Year = time.Now().Year()
	}
	// TODO figure out a better way to do these checks
	if d.Name == "" && Templates[t].Needs["Name"] ||
		d.OneLine == "" && Templates[t].Needs["OneLine"] ||
		d.Title == "" && Templates[t].Needs["Title"] {
		return "", ErrTooFewArgs
	}
	// get template data structure
	templateData, ok := Templates[t]
	if !ok {
		return "", ErrNotFound
	}
	// parse template
	tmpl, err := template.New(t).Parse(templateData.Tmpl)
	if err != nil {
		return "", err
	}
	// execute template against values provided
	err = tmpl.Execute(out, d)
	if err != nil {
		return "", err
	}
	return out.String(), nil
}

// New adds a new license template
// n is uppercase name, all caps
// t is string literal template
// d is short, plain language description
// v is a map[string]bool, one key for each Value field required by the template as per the type Values
func New(n, t, d string, v map[string]bool) error {
	if _, exist := Templates[n]; exist {
		return ErrAlreadyExists
	}
	// TODO check for zero length string in input
	newLicense := License{
		Id:    n,
		Tmpl:  t,
		Desc:  d,
		Needs: v,
	}
	Templates[n] = newLicense
	return nil
}

// Show returns the raw string template of a license or an error if it doesn't exist.
func Show(t string) (string, error) {
	t = strings.ToUpper(t)
	if _, ok := Templates[t]; !ok {
		return "", ErrNotFound
	}
	return Templates[t].Tmpl, nil
}
